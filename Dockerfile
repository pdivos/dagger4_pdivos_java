FROM maven:alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# install maven dependency packages
COPY pom.xml /usr/src/app
RUN mvn -T 1C install && rm -rf target
# run a bare mvn exec:java in order to also download it's deps
RUN mvn exec:java; exit 0

# switch on asserts when running through mvn exec:java
ENV MAVEN_OPTS="-ea"

COPY src /usr/src/app/src
RUN mvn package

RUN mvn exec:java -Dexec.mainClass="com.dagger4.worker.Main" -Dexec.args="test"

RUN mvn exec:java -Dexec.mainClass="com.dagger4.worker.Main" -Dexec.args="dcallables"

COPY resources /usr/src/app/resources
