function opta_decode(data) {
    var data_b64decoded = $jqOpta.base64.decode(data);
    var data_decrypted = $jqOpta.teajs.decrypt(data_b64decoded, "P!Fgob$*LKDF D)(F IDD&P?/");
    var d = new $jqOpta.jDataView(data_decrypted);
    d._littleEndian = !1;
    return d;
}

function opta_decode_feed(feed_id, data) {
    var feed_id = feed_id.toUpperCase();
    var d = opta_decode(data);
    var feed_version = d.getInt8();
    var o = {"feed_version": feed_version,
	     "data": null};
    var decoder_id = feed_id + '_' + feed_version;
    var decoder_fn = $jqOpta['bin_' + decoder_id];
    if (!decoder_fn) {
	return JSON.stringify({"missing_decoder": decoder_id})
    } else {
	decoder_fn(d, null, function(r) {o.data=r})
	return JSON.stringify(o);
    }
 }
