package com.pdivos_java;

import java.util.*;

import com.dagger4.Call;
import com.dagger4.Fiddle;
import com.dagger4.worker.DCallable;
import com.dagger4.core.DFunType;
import com.dagger4.core.DException;

public class DCallables {
    public static int fib_odd(int n) {
        // demonstrates a cross-commit call
        // returns an odd Fibonacci number
        // note: fib_even resides in another commit and is written in Python
        assert n >= 0 && (n%2) == 0 : n;
        if(n==0) return 0;
        Integer fib_2 = (Integer)(new Call("fib_odd", n-2).value);
        Integer fib_1 = (Integer)(new Call("fib_even", n-1).value);
        return fib_2 + fib_1;
    }

    public static int fib_fiddle_odd() {
        // demonstrates a cross-commit call with fiddles
        // returns an odd Fibonacci number by setting n through a Fiddle
        // note: fib_fiddle_even resides in another commit and is written in Python
        int n = (Integer)(new Call("n").value); // this value is set by a Fiddle
        assert n >= 0 && (n%2) == 0 : n;
        if(n==0) return 0;
        Integer fib_1 = null;
        Integer fib_2 = null;
        try (Fiddle d = new Fiddle("n", n-2)) { // override the Fiddle and call recursively
            fib_2 = (Integer)(new Call("fib_fiddle_odd").value);
        }
        try (Fiddle d = new Fiddle("n", n-1)) { // override the Fiddle and call recursively
            fib_1 = (Integer)(new Call("fib_fiddle_even").value);
        }
        return fib_2 + fib_1;
    }

    public static int collatz(int n) {
        if(n == 1) return 0;
        if (n % 2 == 0) n = n / 2;
        else n = 3 * n + 1;
        return ((Integer)(new Call("collatz", n).value)) + 1;
    }

    public static Map<Object, Object> test_dict() {
        Map<Object, Object> m = new HashMap<Object, Object>();
        m.put("a", new Integer(1));
        m.put(new Integer(2), "b");
        m.put(new Boolean(true), null);
        return m;
    }

    public static Set<Object> to_set(Object a, Object b, Object c) {
        Set<Object> s = new HashSet<Object>();
        s.add(a);
        s.add(b);
        s.add(c);
        return s;
    }

    public static Map<String,Object> test_composite0(Object a, Object b) {
        Map<String, Object> m0 = new HashMap<String, Object>();
        m0.put("c0_a", a);
        m0.put("c0_b", b);
        return m0;
    }

    public static Map<String,Object> test_composite1() {
        Map<String, Object> m1 = new HashMap<String, Object>();
        m1.put("c1_a", new Call("test_composite0",new Call("test_dict").value,new Call("to_set",1,2,3).value).value);
        m1.put("c1_b", new Call("to_set",1,2,3).value);
        return m1;
    }

    public static void test_objects() {
        Map<Object, Object> m = new HashMap<Object, Object>();
        m.put("a", new Integer(1));
        m.put(new Integer(2), "b");
        m.put(new Boolean(true), null);

        Set<Object> s = new HashSet<Object>();
        s.add(new Integer(1));
        s.add(new Integer(2));
        s.add(new Integer(3));

        Map<String, Object> m0 = new HashMap<String, Object>();
        m0.put("c0_a", m);
        m0.put("c0_b", s);

        Map<String, Object> m1 = new HashMap<String, Object>();
        m1.put("c1_a", m0);
        m1.put("c1_b", s);

        assert new Call("test_composite1").value.equals(m1);
    }

    public static void test_throw() {
        throw new RuntimeException("Test Exception");
    }

    public static String test_catch() {
        try {
            new Call("test_throw");
        } catch(DException e) {
            System.out.println(e.toString());
            return e.getMessage();
        }
        assert false : "Unreachable code path";
        return null;
    }

    public static void test_exception() {
        assert ((String)new Call("test_catch").value).contains("Test Exception");
        System.out.println("test passed");
    }

    public static List<DCallable> dcallables() {
        List<DCallable> dcallables = new ArrayList<DCallable>();
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "fib_odd"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> args) {
                assert args.size() == 1;
                return new Integer(DCallables.fib_odd((Integer)args.get(0)));
            }
            public String source() {
                return String.join("\n"
                    ,"public static int fib_odd(int n) {"
                    ,"    // demonstrates a cross-commit call"
                    ,"    // returns an odd Fibonacci number"
                    ,"    // note: fib_even resides in another commit and is written in Python"
                    ,"    assert n >= 0 && (n%2) == 0 : n;"
                    ,"    if(n==0) return 0;"
                    ,"    Integer fib_2 = (Integer)(new Call(\"fib_odd\", n-2).value);"
                    ,"    Integer fib_1 = (Integer)(new Call(\"fib_even\", n-1).value);"
                    ,"    return fib_2 + fib_1;"
                    ,"}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "fib_fiddle_odd"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> args) {
                assert args.size() == 0;
                return new Integer(DCallables.fib_fiddle_odd());
            }
            public String source() {
                return String.join("\n"
                    ,"public static int fib_fiddle_odd() {"
                    ,"    // demonstrates a cross-commit call with fiddles"
                    ,"    // returns an odd Fibonacci number by setting n through a Fiddle"
                    ,"    // note: fib_fiddle_even resides in another commit and is written in Python"
                    ,"    int n = (Integer)(new Call(\"n\").value); // this value is set by a Fiddle"
                    ,"    assert n >= 0 && (n%2) == 0 : n;"
                    ,"    if(n==0) return 0;"
                    ,"    Integer fib_1 = null;"
                    ,"    Integer fib_2 = null;"
                    ,"    try (Fiddle _ = new Fiddle(\"n\", n-2)) { // override the Fiddle and call recursively"
                    ,"        fib_2 = (Integer)(new Call(\"fib_fiddle_odd\").value);"
                    ,"    }"
                    ,"    try (Fiddle _ = new Fiddle(\"n\", n-1)) { // override the Fiddle and call recursively"
                    ,"        fib_1 = (Integer)(new Call(\"fib_fiddle_even\").value);"
                    ,"    }"
                    ,"    return fib_2 + fib_1;"
                    ,"}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "collatz"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> args) {
                assert args.size() == 1;
                return new Integer(DCallables.collatz((Integer)args.get(0)));
            }
            public String source() {
                return String.join("\n",
                    "public static int collatz(int n) {",
                    "    if(n == 1) return 0;",
                    "",
                    "    if (n % 2 == 0) n = n / 2;",
                    "    else n = 3 * n + 1;",
                    "    return ((Integer)(new Call(\"collatz\", n).value)) + 1;",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_dict"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> args) {
                assert args.size() == 0;
                return DCallables.test_dict();
            }
            public String source() {
                return String.join("\n",
                    "public static Map<Object, Object> test_dict() {",
                    "    Map<Object, Object> m = new HashMap<Object, Object>();",
                    "    m.put(\"a\", new Integer(1));",
                    "    m.put(new Integer(2), \"b\");",
                    "    m.put(new Boolean(true), null);",
                    "    return m;",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "to_set"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 3;
                return DCallables.to_set(a.get(0), a.get(1), a.get(2));
            }
            public String source() {
                return String.join("\n",
                    "public static Set<Object> to_set(Object a, Object b, Object c) {",
                    "    Set<Object> s = new HashSet<Object>();",
                    "    s.add(a);",
                    "    s.add(b);",
                    "    s.add(c);",
                    "    return s;",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_composite0"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 2;
                return DCallables.test_composite0(a.get(0), a.get(1));
            }
            public String source() {
                return String.join("\n",
                    "public static Map<String,Object> test_composite0(Object a, Object b) {",
                    "    Map<String, Object> m0 = new HashMap<String, Object>();",
                    "    m0.put(\"c0_a\", a);",
                    "    m0.put(\"c0_b\", b);",
                    "    return m0;",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_composite1"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                return DCallables.test_composite1();
            }
            public String source() {
                return String.join("\n",
                    "public static Map<String,Object> test_composite1() {",
                    "    Map<String, Object> m1 = new HashMap<String, Object>();",
                    "    m1.put(\"c1_a\", new Call(\"test_composite0\",new Call(\"test_dict\").value,new Call(\"to_set\",1,2,3).value).value);",
                    "    m1.put(\"c1_b\", new Call(\"to_set\",1,2,3).value);",
                    "    return m1;",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_objects"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                DCallables.test_objects();
                return null;
            }
            public String source() {
                return String.join("\n",
                    "public static void test_objects() {",
                    "    Map<Object, Object> m = new HashMap<Object, Object>();",
                    "    m.put(\"a\", new Integer(1));",
                    "    m.put(new Integer(2), \"b\");",
                    "    m.put(new Boolean(true), null);",
                    "",
                    "    Set<Object> s = new HashSet<Object>();",
                    "    s.add(new Integer(1));",
                    "    s.add(new Integer(2));",
                    "    s.add(new Integer(3));",
                    "",
                    "    Map<String, Object> m0 = new HashMap<String, Object>();",
                    "    m0.put(\"c0_a\", m);",
                    "    m0.put(\"c0_b\", s);",
                    "",
                    "    Map<String, Object> m1 = new HashMap<String, Object>();",
                    "    m1.put(\"c1_a\", m0);",
                    "    m1.put(\"c1_b\", s);",
                    "",
                    "    assert new Call(\"test_composite1\").value.equals(m1);",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "scriptengines"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                OptaNashorn.scriptengines();
                return null;
            }
            public String source() {
                return String.join("\n",
                    "public static void scriptengines() {",
                    "    ScriptEngineManager mgr = new ScriptEngineManager();",
                    "    List<ScriptEngineFactory> factories = mgr.getEngineFactories();",
                    "    for (ScriptEngineFactory factory : factories) {",
                    "        System.out.println(\"ScriptEngineFactory Info\");",
                    "        String engName = factory.getEngineName();",
                    "        String engVersion = factory.getEngineVersion();",
                    "        String langName = factory.getLanguageName();",
                    "        String langVersion = factory.getLanguageVersion();",
                    "        System.out.printf(\"\\tScript Engine: %s (%s)%n\", engName, engVersion);",
                    "        List<String> engNames = factory.getNames();",
                    "        for(String name : engNames) {",
                    "            System.out.printf(\"\\tEngine Alias: %s%n\", name);",
                    "        }",
                    "        System.out.printf(\"\\tLanguage: %s (%s)%n\", langName, langVersion);",
                    "    }",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_nashorn"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                return OptaNashorn.test_nashorn();
            }
            public String source() {
                return String.join("\n",
                    "public static Object test_nashorn() {",
                    "    ScriptEngine engine = new ScriptEngineManager().getEngineByName(\"nashorn\");",
                    "    engine.eval(\"function js_add(a, b) { return a + b; }\nprint('Hello World!');\");",
                    "    return ((Invocable)engine).invokeFunction(\"js_add\", 1, 2);",
                    "}"
                );
            }
        });
        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "opta_decode_feed"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 2;
                return OptaNashorn.opta_decode_feed(a.get(0), a.get(1));
            }
            public String source() {
                return String.join("\n",
                    "public static Object opta_decode_feed(Object feed_id, Object data) {",
                    "    try {",
                    "        ScriptEngine engine = this._engine;",
                    "        return ((Invocable)engine).invokeFunction(\"opta_decode_feed\", feed_id, data);",
                    "    } catch (Exception e) {",
                    "        throw new RuntimeException(e);",
                    "    }",
                    "}"
                );
            }
        });

        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_throw"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                DCallables.test_throw();
                return null;
            }
            public String source() {
                return String.join("\n",
                    "public static void test_throw() {",
                    "    throw new RuntimeException(\"Test Exception\");",
                    "}"
                );
            }
        });

        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_catch"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                return DCallables.test_catch();
            }
            public String source() {
                return String.join("\n",
                    "public static String test_catch() {",
                    "    try {",
                    "        new Call(\"test_throw\");",
                    "    } catch(DException e) {",
                    "        System.out.prinln(e.toString());",
                    "        return e.getMessage();",
                    "    }",
                    "    assert false : \"Unreachable code path\";",
                    "}"
                );
            }
        });

        dcallables.add(new com.dagger4.worker.DCallable() {
            public String func() { return "test_exception"; }
            public DFunType dfuntype() { return DFunType.LAMBDA; }
            public Object run(List<Object> a) {
                assert a.size() == 0;
                DCallables.test_exception();
                return null;
            }
            public String source() {
                return String.join("\n",
                    "public static void test_exception() {",
                    "    assert ((String)new Call(\"test_catch\").value).contains(\"Test Exception\");",
                    "    System.out.println(\"test passed\");",
                    "}"
                );
            }
        });

        return dcallables;
    }
}

