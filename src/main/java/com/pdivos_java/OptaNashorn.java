package com.pdivos_java;

import java.util.*;
import java.io.*;
import javax.script.*;

public class OptaNashorn {
    public static void scriptengines() {
        ScriptEngineManager mgr = new ScriptEngineManager();
        List<ScriptEngineFactory> factories = mgr.getEngineFactories();
        for (ScriptEngineFactory factory : factories) {
            System.out.println("ScriptEngineFactory Info");
            String engName = factory.getEngineName();
            String engVersion = factory.getEngineVersion();
            String langName = factory.getLanguageName();
            String langVersion = factory.getLanguageVersion();
            System.out.printf("\tScript Engine: %s (%s)%n", engName, engVersion);
            List<String> engNames = factory.getNames();
            for(String name : engNames) {
                System.out.printf("\tEngine Alias: %s%n", name);
            }
            System.out.printf("\tLanguage: %s (%s)%n", langName, langVersion);
        }
    }

    public static Object test_nashorn() {
        try {
            String feed_id = "f24";
            String data = new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get("/usr/src/app/resources/f24_641698.b64")), java.nio.charset.StandardCharsets.UTF_8);
            return ((Invocable)engine()).invokeFunction("opta_decode_feed", feed_id, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static ScriptEngine _engine = null;
    private static ScriptEngine engine() {
        if(_engine == null) {
            try {
                ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
                engine.eval("load('nashorn:mozilla_compat.js');");
                final String base_dir = "/usr/src/app/resources/";
                File folder = new File(base_dir);
                engine.eval(new FileReader(base_dir + "env.nashorn.1.2.js"));
                engine.eval(new FileReader(base_dir + "widgets.opta.js"));
                engine.eval(new FileReader(base_dir + "date-en-GB.js"));
                engine.eval(new FileReader(base_dir + "opta_decode_util.js"));
                for (File f : folder.listFiles()) {
                    if (f.isFile() && f.getName().endsWith(".js") && f.getName().startsWith("F") && f.getName().contains("_")) {
                        engine.eval(new FileReader(base_dir + f.getName()));
                    }
                }
                _engine = engine;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return _engine;
    }

    public static Object opta_decode_feed(Object feed_id, Object data) {
        try {
            return ((Invocable)engine()).invokeFunction("opta_decode_feed", feed_id, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}