package com.dagger4.utils;

import java.util.*;
import com.dagger4.core.*;
import java.util.function.Function;

public class UserSerializers {
    private final static byte[] typeid_msgpackable = new byte[]{(byte)255,(byte)'m'};
    private final static byte[] typeid_list = new byte[]{(byte)255,(byte)'l'};
    private final static byte[] typeid_tuple = new byte[]{(byte)255,(byte)'t'};
    private final static byte[] typeid_set = new byte[]{(byte)255,(byte)'s'};
    private final static byte[] typeid_dict = new byte[]{(byte)255,(byte)'d'};
    private final static byte[] typeid_DException = new byte[]{(byte)255,(byte)'e'};

    private static boolean is_msgpackable_primitive(Object obj) {
        if(com.dagger4.utils.Utils.is_primitive(obj)) return true;
        else if(obj instanceof String) return true;
        else if(obj instanceof java.nio.ByteBuffer) return true;
        else if(obj instanceof DFunType) return true;
        else if(obj instanceof DStatus) return true;
        else if(obj instanceof DRef) return true;
        else if(obj instanceof DValue) return true;
        else if(obj instanceof DNodeKey) return true;
        else return false;
    }
    public static byte[] typeid(Object obj) {
        if(is_msgpackable_primitive(obj)) {
            return typeid_msgpackable;
        } else if(obj instanceof List) {
            return typeid_list;
        // } else if(obj instanceof Tuple) {
        //     return typeid_list;
        } else if(obj instanceof Set) {
            return typeid_set;
        } else if(obj instanceof Map) {
            return typeid_dict;
        } else if(obj instanceof DException) {
            return typeid_DException;
        } else {
            throw new RuntimeException();
        }
    }
    private static boolean _shouldnt_dref(Object x) {
        if(com.dagger4.utils.Utils.is_primitive(x)) return true;
        else if(x instanceof DFunType) return true;
        else if(x instanceof DStatus) return true;
        else if(x instanceof String && ((String)x).length() <= 40) return true;
        else return false;
    }
    public static Object serialize(Object obj, Function<Object, DRef> ser) {
        if(is_msgpackable_primitive(obj)) {
            return obj;
        } else if(obj instanceof List) {
            List<Object> msgpackable = new ArrayList<Object>();
            for(Object a : (List<Object>)obj) {
                msgpackable.add(_shouldnt_dref(a) ? a : ser.apply(a));
            }
            return msgpackable;
        // } else if(obj instanceof Tuple) {
        //     List<Object> msgpackable = new ArrayList<Object>();
        //     for(Object a : (List<Object>)obj) {
        //         msgpackable.add(_shouldnt_dref(a) ? a : ser.apply(a));
        //     }
        //     return msgpackable;
        } else if(obj instanceof Set) {
            Set<Object> msgpackable = new HashSet<Object>();
            for(Object a : (Set<Object>)obj) {
                msgpackable.add(_shouldnt_dref(a) ? a : ser.apply(a));
            }
            return msgpackable;
        } else if(obj instanceof Map) {
            Map<Object,Object> msgpackable = new HashMap<Object,Object>();
            for(Map.Entry<Object, Object> entry : ((Map<Object,Object>)obj).entrySet()) {
                Object a = entry.getValue();
                msgpackable.put(entry.getKey(), _shouldnt_dref(a) ? a : ser.apply(a));
            }
            return msgpackable;
        } else if(obj instanceof DException) {
            Map<String,Object> msgpackable = new HashMap<String,Object>();
            Object x = ((DException)obj).stacktrace();
            msgpackable.put("stacktrace", _shouldnt_dref(x) ? x : ser.apply(x));
            x = ((DException)obj).dnodekeys();
            msgpackable.put("dnodekeys", _shouldnt_dref(x) ? x : ser.apply(x));
            return msgpackable;
            
        } else {
            throw new RuntimeException();
        }
    }
    public static Object deserialize(byte[] typeid, Object msgpackable, Function<DRef, Object> deser) {
        if(Arrays.equals(typeid,typeid_msgpackable)) {
            return msgpackable;
        } else if(typeid.equals(typeid_list)) {
            assert (msgpackable instanceof List);
            List<Object> obj = new ArrayList<Object>();
            for(Object a : (List<Object>)msgpackable) {
                obj.add((a instanceof DRef) ? deser.apply((DRef)a) : a);
            }
            return obj;
        } else if(Arrays.equals(typeid,typeid_tuple)) {
            assert (msgpackable instanceof List);
            List<Object> obj = new ArrayList<Object>();
            for(Object a : (List<Object>)msgpackable) {
                obj.add((a instanceof DRef) ? deser.apply((DRef)a) : a);
            }
            return obj;
        } else if(Arrays.equals(typeid,typeid_set)) {
            assert (msgpackable instanceof Set);
            Set<Object> obj = new HashSet<Object>();
            for(Object a : (Set<Object>)msgpackable) {
                obj.add((a instanceof DRef) ? deser.apply((DRef)a) : a);
            }
            return obj;
        } else if(Arrays.equals(typeid,typeid_dict)) {
            Map<Object,Object> obj = new HashMap<Object,Object>();
            for(Map.Entry<Object, Object> entry : ((Map<Object,Object>)msgpackable).entrySet()) {
                Object a = entry.getValue();
                obj.put(entry.getKey(), (a instanceof DRef) ? deser.apply((DRef)a) : a);
            }
            return obj;
        } else if(Arrays.equals(typeid,typeid_DException)) {
            Object x = ((Map<String,Object>)msgpackable).get("stacktrace");
            String stacktrace = (x instanceof DRef) ? (String)deser.apply((DRef)x) : (String)x;
            x = ((Map<String,Object>)msgpackable).get("dnodekeys");
            ArrayList<DNodeKey> dnodekeys = (x instanceof DRef) ? (ArrayList<DNodeKey>)deser.apply((DRef)x) : (ArrayList<DNodeKey>)x;
            return new DException(stacktrace, dnodekeys);
        } else {
            throw new RuntimeException("Unknown typeid: " + Utils.bytesToHex(typeid));
        }
    }
}
