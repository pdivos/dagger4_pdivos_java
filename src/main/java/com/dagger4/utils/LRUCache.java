package com.dagger4.utils;

import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.function.BiConsumer;

// https://stackoverflow.com/questions/23772102/lru-cache-in-java-with-generics-and-o1-operations

public class LRUCache<K,V> {
    private final int capacity;
    private final LinkedHashMap<K,V> map = new LinkedHashMap<>();
    private final BiConsumer<K,V> on_evicted;

    public LRUCache(int capacity, BiConsumer<K,V> on_evicted) {
        this.capacity = capacity;
        this.on_evicted = on_evicted;
    }
    public boolean contains(K key) {
        return this.map.containsKey(key);
    }
    public int size() {
        return this.capacity;
    }
    public V get(K key) {
        assert this.map.containsKey(key);
        V value = this.map.get(key);
        this.set(key, value);
        return value;
    }
    public void set(K key, V value) {
        if (this.map.containsKey(key)) {
            this.map.remove(key);
        } else if (this.map.size() == this.capacity) {
            Iterator<K> it = this.map.keySet().iterator();
            K removed_key = it.next();
            V removed_value = this.map.get(removed_key);
            it.remove();
            if(this.on_evicted != null) {
                this.on_evicted.accept(removed_key, removed_value);
            }
        }
        map.put(key, value);
    }
}