package com.dagger4.utils;

import java.util.*;
import java.util.function.Function;
import com.dagger4.core.*;
import java.util.function.Function;

public class TreeSerializer {
    private final ObjStore objstore = new ObjStore();
    private final DRefStore dvaluestore;
    public TreeSerializer(int lru_size) {
        this.dvaluestore = new DRefStore(lru_size, (dref,v)->{
            this.objstore.remove(dref);
        });
    }
    public DRef serialize(Object obj) {
        if(this.objstore.contains_obj(obj)) {
            return this.objstore.get_dref(obj);
        } else {
            byte[] typeid = UserSerializers.typeid(obj);
            Set<DRef> child_drefs = new HashSet<DRef>();
            final TreeSerializer dis = this;
            Function<Object,DRef> ser = new Function<Object,DRef>() {
                public DRef apply(Object obj) {
                    DRef dref = dis.serialize(obj);
                    child_drefs.add(dref);
                    return dref;
                }
            };
            Object msgpackable = UserSerializers.serialize(obj, ser);
            byte[] buf = Msgpack.serialize(msgpackable);
            DValue dvalue = new DValue(typeid, buf, child_drefs);
            DRef dref = DRef.from_buf(buf);
            this.dvaluestore.set(dref, dvalue);
            this.objstore.set(dref, obj);
            return dref;
        }
    }
    public Object deserialize(DRef dref) {
        if(this.objstore.contains(dref)) {
            return this.objstore.get(dref);
        } else if(this.dvaluestore.contains(dref)) {
            DValue dvalue = this.dvaluestore.get(dref);
            Object msgpackable = Msgpack.deserialize(dvalue.buf());
            final TreeSerializer dis = this;
            Function<DRef,Object> deser = new Function<DRef,Object>() {
                public Object apply(DRef dref) {
                    assert dvalue.drefs().contains(dref);
                    return dis.deserialize(dref);
                }
            };
            Object obj = UserSerializers.deserialize(dvalue.typeid(), msgpackable, deser);
            this.objstore.set(dref, obj);
            return obj;
        } else {
            throw new RuntimeException("cannot find dref: " + dref.toString());
        }
    }
    public DRefStore getDRefStore() {
        return this.dvaluestore;
    }

    public Object ensure(Object obj) {
        // ensure:
        //     primitive -> primitive
        //     DRef      -> DRef
        //     object    -> DRef
        if(com.dagger4.utils.Utils.is_primitive(obj) || (obj instanceof DRef)) {
            return obj;
        } else {
            return this.serialize(obj);
        }
    }

    public List<Object> ensure_list(List<Object> args) {
        if(args.size()==0) return args;
        List<Object> ret = new ArrayList<Object>(args.size());
        for(Object arg : args) {
            ret.add(this.ensure(arg));
        }
        return ret;
    }

    public List<Object> ensure_list(Object[] args) {
        List<Object> ret = new ArrayList<Object>(args.length);
        for(Object arg : args) {
            ret.add(this.ensure(arg));
        }
        return ret;
    }

    public Object resolve(Object obj) {
        // resolve:
        //     primitive -> primitive
        //     DRef      -> object
        //     object    -> Exception
        if(obj instanceof DRef) {
            return this.deserialize((DRef)obj);
        } else {
            assert com.dagger4.utils.Utils.is_primitive(obj);
            return obj;
        }
    }

    public List<Object> resolve_list(List<Object> args) {
        if(args.size()==0) return args;
        List<Object> ret = new ArrayList<Object>(args.size());
        for(Object arg : args) {
            ret.add(this.resolve(arg));
        }
        return ret;
    }

    public static void test() {
        TreeSerializer ts = new TreeSerializer(1024);
        ts.serialize(new Integer(13));
    }
}
