package com.dagger4.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ServerHandshake;

import com.dagger4.core.*;
import com.dagger4.utils.TreeSerializer;

public class Client extends WebSocketClient {
	private final int _LRU_CACHE_SIZE = 1024 * 128;
	private BlockingQueue<ByteBuffer> queue = new LinkedBlockingQueue();
	private long msg_id_autoinc = 0;
	private final String worker_id;
	public final TreeSerializer treeserializer;
	public Client(String worker_id, String uri) {
		super(stringToUri(uri));
		try {
			this.connectBlocking();
		} catch(InterruptedException e) {
			throw new RuntimeException(e);
		}
		this.worker_id = worker_id;
		this.treeserializer = new TreeSerializer(_LRU_CACHE_SIZE);
	}

	private static URI stringToUri(String s) {
		try {
			return new URI(s);
		} catch(URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
	}

	@Override
	public void onMessage(String message) {
	}

	@Override
	public void onMessage(ByteBuffer message) {
		queue.add(message);
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		// The codecodes are documented in class org.java_websocket.framing.CloseFrame
		// System.out.println("Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: " + reason);
	}

	@Override
	public void onError(Exception ex) {
		ex.printStackTrace();
		// if the error is fatal then onClose will be called additionally
	}

	private byte[] receiveBlocking() {
		List<ByteBuffer> responses = new ArrayList<ByteBuffer>();
		try {
			responses.add(queue.take());
		} catch(InterruptedException e) {
			throw new RuntimeException(e);
		}
		while(true) {
			ByteBuffer r = queue.poll();
			if(r == null) break;
			responses.add(r);
		}
		int size = 0;
		for(ByteBuffer b : responses) {
			size += b.array().length;
		}
		byte[] resp = new byte[size];
		int offset = 0;
		for(ByteBuffer b : responses) {
			byte[] bb = b.array();
			for(int i=0; i<bb.length; i++) {
				resp[offset] = bb[i];
				offset ++;
			}
		}
		return resp;
	}

	private Object call(String method, Object ... args) {
		Object[] tmp = new Object[args.length+1];
		tmp[0] = method;
		for(int i=0; i<args.length; i++) tmp[i+1] = args[i];
		MyLog.d(tmp);
		List<Object> _args = new ArrayList<Object>(args.length);
		for(Object arg : args) _args.add(arg);
		Map<String,Object> __drefstore_c2s__ = new TreeMap<String,Object>();
		Map<String,Object> changes = this.treeserializer.getDRefStore().pop_changes();
		__drefstore_c2s__.put("worker_id", this.worker_id);
		__drefstore_c2s__.put("changes", changes);
		// MyLog.d("__drefstore_c2s__.dropped_keys", changes.get("dropped_keys"));
		// MyLog.d("__drefstore_c2s__.new_keyvalues", changes.get("new_keyvalues"));
		DRequest req = new DRequest(method, _args, this.msg_id_autoinc, __drefstore_c2s__);
		send(Msgpack.serialize(req));
		this.msg_id_autoinc++;
		if(!method.equals("complete")) {
			byte[] resp = receiveBlocking();
			DResponse ret = (DResponse)Msgpack.deserialize(resp);
			assert ret.msg_id().intValue() == this.msg_id_autoinc-1 : ret.msg_id().intValue();
			assert ret.__drefstore_s2c__().containsKey("new_keyvalues") && ret.__drefstore_s2c__().containsKey("required_keys");
			this.treeserializer.getDRefStore().update(
				(Map<DRef, DValue>)ret.__drefstore_s2c__().get("new_keyvalues"),
				(List<DRef>)ret.__drefstore_s2c__().get("required_keys")
			);
			Object response = ret.response();
			if(response instanceof com.dagger4.core.DServerError) {
				throw (com.dagger4.core.DServerError)response;
			}
			MyLog.d(response);
			return response;
		} else {
			// don't wait for response for noresponse_methods
			return null;
		}
	}

    public void err() {
		this.call("err");
	}

	public String ping() {
		return (String)this.call("ping");
	}

    public byte[] kvstore_get(byte[] key) {
        return ((ByteBuffer)this.call("kvstore_get", ByteBuffer.wrap(key))).array();
	}

    public void kvstore_set(byte[] key, byte[] value) {
        this.call("kvstore_set", ByteBuffer.wrap(key), ByteBuffer.wrap(value));
	}

    public Map<String,Object> get_work(String drepo, double timeout) {
        return (Map<String,Object>)this.call("get_work", drepo, new Double(timeout));
	}

    public Map<String,Object> call(DNodeKeyRunning callee, DNodeKeyRunning caller) {
        return (Map<String,Object>)this.call("call", callee, caller);
	}

    public void complete(DNodeKeyRunning dnodekeyrunning, Boolean is_success, Object value, Object stdout, Object stderr) {
        this.call("complete", dnodekeyrunning, is_success, value, stdout, stderr);
	}
	
}