package com.dagger4.utils;

import java.io.IOException;

import org.joda.time.DateTime;

public class MyLog {

	private static boolean addTimestamp = true;
	private static boolean addCallerFunction = true;

	private static String getCallerFunctionName() {
		final StackTraceElement[] els = Thread.currentThread().getStackTrace();
		for (int i = 1; i < els.length; i++) {
			if (!els[i].getClassName().equals(MyLog.class.getName())) {
				return els[i].toString();
			}
		}
		return "";
	}

	private static void addBeginning(final StringBuilder sb) {
		if (addTimestamp && addCallerFunction) {
			sb.append(DateTime.now()).append(" ").append(getCallerFunctionName()).append(": ");
		} else if (addTimestamp) {
			sb.append(DateTime.now()).append(": ");
		} else if (addCallerFunction) {
			sb.append(getCallerFunctionName()).append(": ");
		}
	}

	private static String s(final String msg) {
		final StringBuilder sb = new StringBuilder();
		addBeginning(sb);
		sb.append(msg.trim()).append("\n");
		return sb.toString();
	}

	private static String s(final String[] msgs) {
		final StringBuilder sb = new StringBuilder();
		for (final String msg : msgs) {
			if (msg != null) {
				sb.append(msg.trim());
			} else {
				sb.append("null");
			}
			sb.append(" ");
		}
		return s(sb.toString());
	}

	private static String s(final Object... msgs) {
		final String arr[] = new String[msgs.length];
		int i = 0;
		for (final Object msg : msgs) {
			if (msg != null) {
				arr[i] = msg.toString();
			} else {
				arr[i] = "null";
			}
			i++;
		}
		return s(arr);
	}

	private static String s(final Object msg) {
		return s(msg == null ? "null" : msg.toString());
	}

	public static void d(final Object msg) {
		System.out.print(s(msg));
	}

	public static void d(final Object... msgs) {
		System.out.print(s(msgs));
	}

	public static void d(final String msg) {
		System.out.print(s(msg));
	}

	public static void d(final String[] msgs) {
		System.out.print(s(msgs));
	}

}