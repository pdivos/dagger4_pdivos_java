package com.dagger4.utils;

import com.dagger4.core.DRef;
import com.dagger4.core.DValue;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.function.BiConsumer;

public class DRefStore {
    private final ArrayList<DRef> dropped_keys = new ArrayList<DRef>();
    private final HashMap<DRef, DValue> new_keyvalues = new HashMap<DRef, DValue>();
    private final LRUCache<DRef,DValue> dref_to_dvalue;
    public DRefStore(int lru_size, BiConsumer<DRef,DValue> on_evicted) {
        this.dref_to_dvalue = new LRUCache<DRef,DValue>(lru_size, (k,v)->{
            this.dropped_keys.add(k);
            if(on_evicted!=null) on_evicted.accept(k,v);
        });
    }
    public DValue get(DRef dref) {
        return this.dref_to_dvalue.get(dref);
    }
    public void set(DRef key, DValue value) {
        if(!this.dref_to_dvalue.contains(key)) {
            this.dref_to_dvalue.set(key, value);
            this.new_keyvalues.put(key, value);
        } else {
            assert this.dref_to_dvalue.get(key).buf().equals(value.buf());
        }
    }
    public boolean contains(DRef key) {
        return this.dref_to_dvalue.contains(key);
    }
    public void update(Map<DRef, DValue> new_keyvalues, List<DRef> required_keys) {
        // update called on the new set of keys received from server
        // important:
        //     anything added through update won't appear in pop_new_keyvalues
        //     keys removed due to stuff added here will apper in dropped_keys
        // new_keyvalues: dict of new kv pairs to be updated
        // required_keys: list of existing keys that should not be removed during update
        int len_dropped_keys = this.dropped_keys.size();
        assert new_keyvalues.size() + required_keys.size() <= this.dref_to_dvalue.size() : new_keyvalues.size() + " + " + required_keys.size() + " <= " + this.dref_to_dvalue.size();
        for(DRef k : required_keys) {
            this.dref_to_dvalue.get(k);
        }
        for (Map.Entry<DRef, DValue> entry : new_keyvalues.entrySet()) {
            this.dref_to_dvalue.set(entry.getKey(),entry.getValue());
        }
    }
    public Map<String,Object> pop_changes() {
        // returns list of dropped keys and resets to empty
        // returns list of new_keyvalues and resets to empty
        ArrayList<DRef> dropped_keys = new ArrayList<DRef>(this.dropped_keys);
        this.dropped_keys.clear();
        HashMap<DRef,DValue> new_keyvalues = new HashMap<DRef,DValue>(this.new_keyvalues);
        this.new_keyvalues.clear();
        Map<String,Object> res = new HashMap<String,Object>();
        res.put("dropped_keys",dropped_keys);
        res.put("new_keyvalues",new_keyvalues);
        return res;
    }
}


