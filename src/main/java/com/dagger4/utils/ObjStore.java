package com.dagger4.utils;

import java.util.Map;
import java.util.HashMap;
import com.dagger4.core.DRef;

public class ObjStore {
    private final Map<DRef,Object> dref_to_obj = new HashMap<DRef,Object>();
    private final Map<Object,DRef> obj_to_dref = new HashMap<Object,DRef>();
    public void set(DRef dref, Object obj) {
        this.dref_to_obj.put(dref,obj);
        this.obj_to_dref.put(obj,dref);
    }
    public Object get(DRef dref) {
        assert this.dref_to_obj.containsKey(dref);
        return this.dref_to_obj.get(dref);
    }
    public boolean contains(DRef dref) {
        return this.dref_to_obj.containsKey(dref);
    }
    public void remove(DRef dref) {
        assert this.dref_to_obj.containsKey(dref);
        Object obj = this.dref_to_obj.get(dref);
        if(this.contains_obj(obj)) {
            this.obj_to_dref.remove(obj);
        }
        this.dref_to_obj.remove(dref);
    }
    public boolean contains_obj(Object obj) {
        return this.obj_to_dref.containsKey(obj);
    }
    public DRef get_dref(Object obj) {
        assert this.obj_to_dref.containsKey(obj);
        return this.obj_to_dref.get(obj);
    }
} 