package com.dagger4.utils;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.function.*;

import org.msgpack.core.MessagePack;
import org.msgpack.core.MessagePack.PackerConfig;
import org.msgpack.core.MessagePack.UnpackerConfig;
import org.msgpack.core.MessageBufferPacker;
import org.msgpack.core.MessageFormat;
import org.msgpack.core.MessagePacker;
import org.msgpack.core.MessageUnpacker;
import org.msgpack.value.ArrayValue;
import org.msgpack.value.MapValue;
import org.msgpack.value.ExtensionValue;
import org.msgpack.core.ExtensionTypeHeader;
import org.msgpack.value.FloatValue;
import org.msgpack.value.IntegerValue;
import org.msgpack.value.Value;

import com.dagger4.core.*;

public class Msgpack {

    private static class Serializer implements Comparable<Serializer>{
        public final double priority;
        public final Predicate<Object> pred;
        public final byte[] typeid;
        public final Function<Object, byte[]> ser;
        public Serializer(double priority, Predicate<Object> pred, byte[] typeid, Function<Object, byte[]> ser) {
            this.priority = priority;
            this.pred = pred;
            this.typeid = typeid;
            this.ser = ser;
        }
        @Override 
        public int compareTo(Serializer o) {
            double diff = this.priority - o.priority;
            return diff < 0 ? -1 : (diff > 0 ? 1 : 0);
        }
    }

    private static class Deserializer {
        public final byte[] typeid;
        public final Function<byte[], Object> deser;
        public Deserializer(byte[] typeid, Function<byte[], Object> deser) {
            this.typeid = typeid;
            this.deser = deser;
        }
    }

    private static ArrayList<Msgpack.Serializer> serializers = new ArrayList<Msgpack.Serializer>();
    private static ArrayList<Msgpack.Deserializer> deserializers = new ArrayList<Msgpack.Deserializer>();

    public static void register_serializer(double priority, Predicate<Object> pred, byte[] typeid, Function<Object, byte[]> ser) {
        Msgpack.serializers.add(new Serializer(priority, pred, typeid, ser));
        java.util.Collections.sort(Msgpack.serializers);
    }

    private static boolean startsWith(byte[] source, byte[] match) {
        if (match.length > (source.length)) return false;
        for (int i = 0; i < match.length; i++) if (source[i] != match[i]) return false;
        return true;
    }

    private static boolean eq(byte[] arr1, int offset1, byte[] arr2, int offset2, int len) {
        for(int i=0; i<len; i++) {
            if(arr1.length <= i + offset1) return false;
            if(arr2.length <= i + offset2) return false;
            if(arr1[i+offset1]!=arr2[i+offset2]) return false;
        }
        return true;
    }

    public static void register_deserializer(byte[] typeid, Function<byte[], Object> deser) {
        for(Deserializer el : deserializers) {
            assert !startsWith(el.typeid, typeid) && !startsWith(typeid, el.typeid) : "typeid collision";
        }
        Msgpack.deserializers.add(new Deserializer(typeid, deser));
    }

    static {
        Msgpack.register_dagger4_serializers();
        Msgpack.register_dagger4_deserializers();
    }

    private static void register_dagger4_serializers() {
        register_serializer(0, o -> o instanceof Set, new byte[]{EXT_TYPE_CORE, EXT_TYPE_CORE_SET}, o -> {
            ArrayList<byte[]> b_list = new ArrayList<byte[]>();
            Set<Object> set = (Set<Object>)o;
            int length = 0;
            for(Object _o : set) {
                byte[] b = serialize(_o);
                b_list.add(b);
                length += b.length;
            }
            java.util.Collections.sort(b_list, new ByteArrayComparator());
            List<Object> list = new ArrayList<Object>();
            return serialize(list);
        });
        /*
        // TODO implenent all this
        if(o instanceof Set) {
        } else if(o instanceof DStatus) {
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,2);
            byte[] payload = new byte[2];
            payload[0] = EXT_TYPE_CORE_DSTATUS;
            boolean found = false;
            for(int i=0; i<DStatus.values().length; i++)
                if(DStatus.values()[i] == o) {
                    found = true;
                    payload[1] = (byte)i;
                }
            assert found;
            packer.writePayload(payload);
        } else if(o instanceof DFunType) {
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,2);
            byte[] payload = new byte[2];
            payload[0] = EXT_TYPE_CORE_DFUNTYPE;
            boolean found = false;
            for(int i=0; i<DFunType.values().length; i++)
                if(DFunType.values()[i] == o) {
                    found = true;
                    payload[1] = (byte)i;
                }
            assert found;
            packer.writePayload(payload);
        } else if(o instanceof DRef) {
            byte[] payload = ((DRef)o).ref;
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DREF});
            packer.writePayload(payload);
        } else if(o instanceof DValue) {
            DValue dvalue = (DValue)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(ByteBuffer.wrap(dvalue.typeid()));
            tmp.add(ByteBuffer.wrap(dvalue.buf()));
            tmp.add(dvalue.drefs());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DVALUE});
            packer.writePayload(payload);
        } else if(o instanceof DCall) {
            DCall dcall = (DCall)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(dcall.func());
            tmp.add(dcall.args());
            tmp.add(new Long(dcall.ts()));
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DCALL});
            packer.writePayload(payload);
        } else if(o instanceof Fiddle) {
            Fiddle fiddle = (Fiddle)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(fiddle.dcall());
            tmp.add(fiddle.value());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_FIDDLE});
            packer.writePayload(payload);
        } else if(o instanceof Fiddles) {
            Fiddles fiddles = ((Fiddles)o);
            TreeSet<ByteBuffer> treeset = new TreeSet<ByteBuffer>();
            int length = 0;
            for (Fiddle f : fiddles) {
                byte[] value = serialize(f);
                length += value.length;
                treeset.add(ByteBuffer.wrap(value));
            }
            // pack from TreeMap to keep order
            length += _array_header_size(fiddles.size());
            length += 1;
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,length);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_FIDDLES});
            packer.packArrayHeader(fiddles.size());
            for (ByteBuffer entry : treeset) {
                byte[] value = entry.array();
                packer.writePayload(value);
            }
        } else if(o instanceof DNodeKey) {
            DNodeKey k = (DNodeKey)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(k.dcall());
            tmp.add(k.fiddles());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DNODEKEY});
            packer.writePayload(payload);
        } else if(o instanceof DNodeKeyRunning) {
            DNodeKeyRunning k = (DNodeKeyRunning)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(k.dcommitset());
            tmp.add(k.dnodekey());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DNODEKEYRUNNING});
            packer.writePayload(payload);
        } else if(o instanceof DNodeKeyCompleted) {
            DNodeKeyCompleted k = (DNodeKeyCompleted)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(k.deep_dcallable_hash());
            tmp.add(k.dnodekey());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DNODEKEYCOMPLETED});
            packer.writePayload(payload);
        } else if(o instanceof DNodeResult) {
            DNodeResult k = (DNodeResult)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(k.is_success());
            tmp.add(k.value());
            tmp.add(k.stdout());
            tmp.add(k.stderr());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DNODERESULT});
            packer.writePayload(payload);
        } else if(o instanceof DRequest) {
            DRequest k = (DRequest)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(k.method());
            tmp.add(k.args());
            tmp.add(k.msg_id());
            tmp.add(k.__drefstore_c2s__());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DREQUEST});
            packer.writePayload(payload);
        } else if(o instanceof DResponse) {
            DResponse k = (DResponse)o;
            List<Object> tmp = new ArrayList<Object>();
            tmp.add(k.response());
            tmp.add(k.msg_id());
            tmp.add(k.__drefstore_s2c__());
            byte[] payload = serialize(tmp);
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DRESPONSE});
            packer.writePayload(payload);
        } else if(o instanceof DServerError) {
            byte[] payload = serialize(((DServerError)o).getMessage());
            packer.packExtensionTypeHeader(EXT_TYPE_CORE,payload.length+1);
            packer.writePayload(new byte[]{EXT_TYPE_CORE_DERROR});
            packer.writePayload(payload);
        } else {
            throw new RuntimeException("unable to serialize: " + o.toString());
        }
        */
    }

    private static void register_dagger4_deserializers() {
        register_deserializer(new byte[]{EXT_TYPE_CORE, EXT_TYPE_CORE_SET}, buf -> {
            List<Object> list = (List<Object>)deserialize(buf);
            HashSet<Object> set = new HashSet<Object>();
            for(Object el : list) {
                assert el instanceof ByteBuffer : el;
                byte[] _buf = ((ByteBuffer)el).array();
                Object obj = deserialize(_buf);
                set.add(obj);
            }
            return set;
        });
        /*
        if(ext_type == EXT_TYPE_CORE) {
            assert payload.length >= 1;
            byte t = payload[0];
            if(t == EXT_TYPE_CORE_SET) {
                return new HashSet<Object>((List<Object>)deserialize(payload, 1));
            } else if(t == EXT_TYPE_CORE_DSTATUS) {
                assert payload.length == 2;
                return DStatus.values()[payload[1]];
            } else if(t == EXT_TYPE_CORE_DFUNTYPE) {
                assert payload.length == 2;
                return DFunType.values()[payload[1]];
            } else if(t == EXT_TYPE_CORE_TUPLE) {
                // python tuple is packed as a list so we can just return.
                // no corresponding packer because we have no tuple in Java
                return deserialize(payload, 1);
            } else if(t == EXT_TYPE_CORE_DREF) {
                return new DRef(Arrays.copyOfRange(payload, 1, payload.length));
            } else if(t == EXT_TYPE_CORE_DVALUE) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                assert list.size() == 3;
                byte[] typeid = ((ByteBuffer)list.get(0)).array();
                byte[] buf = ((ByteBuffer)list.get(1)).array();
                Set<DRef> drefs = (Set<DRef>)list.get(2);
                return new DValue(typeid, buf, drefs);
            } else if(t == EXT_TYPE_CORE_DCALL) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                assert list.size() == 3;
                String func = (String)list.get(0);
                List<Object> args = (List<Object>)list.get(1);
                long ts = (list.get(2) instanceof Long) ? ((Long)list.get(2)).longValue() : ((Integer)list.get(2)).intValue();
                return new DCall(func, args, ts);
            } else if(t == EXT_TYPE_CORE_FIDDLE) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                assert list.size() == 2;
                DCall dcall = (DCall)list.get(0);
                Object value = (Object)list.get(1);
                return new Fiddle(dcall, value);
            } else if(t == EXT_TYPE_CORE_FIDDLES) {
                List<Fiddle> fiddles = (ArrayList<Fiddle>)deserialize(payload, 1);
                return new Fiddles(fiddles);
            } else if(t == EXT_TYPE_CORE_DNODEKEY) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                assert list.size() == 2;
                DCall dcall = (DCall)list.get(0);
                Fiddles fiddles = (Fiddles)list.get(1);
                return new DNodeKey(dcall, fiddles);
            } else if(t == EXT_TYPE_CORE_DNODEKEYRUNNING) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                assert list.size() == 2;
                String dcommitset = (String)list.get(0);
                DNodeKey dnodekey = (DNodeKey)list.get(1);
                return new DNodeKeyRunning(dcommitset, dnodekey);
            } else if(t == EXT_TYPE_CORE_DNODEKEYCOMPLETED) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                assert list.size() == 2;
                DNodeKey dnodekey = (DNodeKey)list.get(1);
                DRef deep_dcallable_hash = (DRef)list.get(0);
                return new DNodeKeyCompleted(deep_dcallable_hash, dnodekey);
            } else if(t == EXT_TYPE_CORE_DNODERESULT) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                return new DNodeResult((Boolean)list.get(0), list.get(1), list.get(2), list.get(3));
            } else if(t == EXT_TYPE_CORE_DREQUEST) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                return new DRequest((String)list.get(0), (List<Object>)list.get(1), (Long)list.get(2), (Map<String,Object>)list.get(3));
            } else if(t == EXT_TYPE_CORE_DRESPONSE) {
                List<Object> list = (List<Object>)deserialize(payload, 1);
                return new DResponse(list.get(0), (Integer)list.get(1), (Map<String,Object>)list.get(2));
            } else if(t == EXT_TYPE_CORE_DERROR) {
                return new DServerError((String)deserialize(payload, 1));
            } else {
                throw new RuntimeException("unknown core extension type: " + String.format("%02X", t));
            }
        } else {
            throw new RuntimeException("unknown extension type: " + String.format("%02X", ext_type));
        }
        */

    }

    private static final byte EXT_TYPE_CORE = 0x40; // msgpack ext type of 0x40 denotes a dagger4 core object
    private static final byte EXT_TYPE_CORE_SET = (byte)'S';
    private static final byte EXT_TYPE_CORE_TUPLE = (byte)'T';
    private static final byte EXT_TYPE_CORE_DSTATUS = (byte)'s';
    private static final byte EXT_TYPE_CORE_DFUNTYPE = (byte)'F';
    private static final byte EXT_TYPE_CORE_DREF = (byte)'R';
    private static final byte EXT_TYPE_CORE_DVALUE = (byte)'V';
    private static final byte EXT_TYPE_CORE_DCALL = (byte)'c';
    private static final byte EXT_TYPE_CORE_FIDDLE = (byte)'d';
    private static final byte EXT_TYPE_CORE_FIDDLES = (byte)'D';
    private static final byte EXT_TYPE_CORE_DNODEKEY = (byte)'j';
    private static final byte EXT_TYPE_CORE_DNODEKEYRUNNING = (byte)'k';
    private static final byte EXT_TYPE_CORE_DNODEKEYCOMPLETED = (byte)'K';
    private static final byte EXT_TYPE_CORE_DNODERESULT = (byte)'r';
    private static final byte EXT_TYPE_CORE_DERROR = (byte)'E';
    private static final byte EXT_TYPE_CORE_DREQUEST = (byte)'Q';
    private static final byte EXT_TYPE_CORE_DRESPONSE = (byte)'P';

    /*
    * pass in user type de/serializers here
    * when encountering a user type, tries these in sequence, if none work throws exception
    */
    public static byte[] serialize(Object o) {
        try {
            MessageBufferPacker packer = MessagePack.newDefaultBufferPacker();
            if(o == null) {
                packer.packNil();
            }
            else if(o instanceof Boolean) {
                packer.packBoolean(((Boolean)o).booleanValue());
            }
            else if(o instanceof Byte) {
                packer.packByte(((Byte)o).byteValue());
            }
            else if(o instanceof Short) {
                packer.packShort(((Short)o).shortValue());
            }
            else if(o instanceof Integer) {
                packer.packInt(((Integer)o).intValue());
            }
            else if(o instanceof Long) {
                packer.packLong(((Long)o).longValue());
            }
            else if(o instanceof BigInteger) {
                packer.packBigInteger((BigInteger)o);
            }
            else if(o instanceof Float) {
                packer.packFloat(((Float)o).floatValue());
            }
            else if(o instanceof Double) {
                packer.packDouble(((Double)o).doubleValue());
            }
            else if(o instanceof String) {
                packer.packString((String)o);
            }
            else if(o instanceof List) {
                List<Object> l = (List<Object>)o;
                packer.packArrayHeader(l.size());
                for(Object _o : l) {
                    packer.writePayload(serialize(_o));
                }
            }
            else if(o instanceof Map) {
                // pack keys, values and move them to TreeMap to have them sorted
                Map<Object,Object> map = (Map<Object,Object>)o;
                TreeMap<ByteBuffer, ByteBuffer> TreeMap = new TreeMap<ByteBuffer, ByteBuffer>();
                for (Map.Entry<Object, Object> entry : map.entrySet()) {
                    ByteBuffer key = ByteBuffer.wrap(serialize(entry.getKey()));
                    ByteBuffer value = ByteBuffer.wrap(serialize(entry.getValue()));
                    TreeMap.put(key, value);
                }
                // pack from TreeMap to keep order
                packer.packMapHeader(map.size());
                for (Map.Entry<ByteBuffer, ByteBuffer> entry : TreeMap.entrySet()) {
                    byte[] key = entry.getKey().array();
                    byte[] value = entry.getValue().array();
                    packer.writePayload(key);
                    packer.writePayload(value);
                }
            }
            else if(o instanceof ByteBuffer) {
                byte[] arr = ((ByteBuffer)o).array();
                packer.packBinaryHeader(arr.length);
                packer.writePayload(arr);
            }
            else {
                packExtensionType(packer, o);
            }
            byte[] msg = packer.toByteArray();
            packer.close();
            return msg;
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Object deserialize(MessageUnpacker unpacker) throws IOException {
        assert unpacker.hasNext();
        final MessageFormat format = unpacker.getNextFormat();
        int length;
        switch (format.getValueType()) {
            case NIL:
                assert unpacker.unpackValue().isNilValue();
                return null;
            case BOOLEAN:
                return new Boolean(unpacker.unpackValue().asBooleanValue().getBoolean());
            case INTEGER:
                IntegerValue iv = unpacker.unpackValue().asIntegerValue();
                if (iv.isInIntRange()) {
                    return new Integer(iv.toInt());
                }
                else if (iv.isInLongRange()) {
                    return new Long(iv.toLong());
                }
                else {
                    return iv.toBigInteger();
                }
            case FLOAT:
                // float f = v.asFloatValue().toFloat();   // use as float
                return new Double(unpacker.unpackValue().asFloatValue().toDouble());
            case STRING:
                return unpacker.unpackValue().asStringValue().asString();
            case BINARY:
                return ByteBuffer.wrap(unpacker.unpackValue().asBinaryValue().asByteArray());
            case ARRAY:
                length = unpacker.unpackArrayHeader();
                ArrayList<Object> arr = new ArrayList<Object>(length);
                for (int i=0; i<length; i++) {
                    arr.add(deserialize(unpacker));
                }
                return arr;
            case MAP:
                length = unpacker.unpackMapHeader();
                HashMap<Object,Object> map = new HashMap<Object,Object>();
                for (int i=0; i<length; i++) {
                    map.put(deserialize(unpacker),deserialize(unpacker));
                }
                return map;
            case EXTENSION:
                // throw new RuntimeException("not implemented");
                ExtensionTypeHeader extHeader = unpacker.unpackExtensionTypeHeader();
                byte type = extHeader.getType();
                length = extHeader.getLength();
                byte[] payload = new byte[length];
                unpacker.readPayload(payload);
                for(Deserializer el : deserializers) {
                    System.out.println(Utils.bytesToHex(el.typeid));
                    System.out.println(Utils.bytesToHex(payload));
                    System.out.println(500);
                    if(el.typeid[0] == type && eq(payload, 1, el.typeid, 1, el.typeid.length-1)) {
                        System.out.println(501);
                        byte[] buf = Arrays.copyOfRange(payload, el.typeid.length-1, payload.length);
                        return el.deser.apply(buf);
                    }
                }
        }
        throw new RuntimeException("invalid code path 506");
    }
    public static Object deserialize(byte[] msg) {
        return deserialize(msg, 0);
    }
    private static Object deserialize(byte[] msg, int offset) {
        try {
            MessageUnpacker unpacker = MessagePack.newDefaultUnpacker(msg, offset, msg.length - offset);
            return deserialize(unpacker);
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class ByteArrayComparator implements java.util.Comparator<byte[]> {
        public int compare(byte[] left, byte[] right) {
            for (int i = 0, j = 0; i < left.length && j < right.length; i++, j++) {
                int a = (left[i] & 0xff);
                int b = (right[j] & 0xff);
                if (a != b) {
                    return a - b;
                }
            }
            return left.length - right.length;
        }
    }

    private static int _array_header_size(int n) {
        assert n >= 0 : n;
        return (n <= 0x0f) ? 1 : (n <= 0xffff ? 3 : (n <= 0xffffffff ? 5 : -1));
    }

    private static void packExtensionType(MessageBufferPacker packer, Object o) throws IOException {
        for(Serializer el : serializers) {
            if(el.pred.test(o)) {
                final byte[] typeid = el.typeid;
                final byte code = typeid[0];
                final byte[] res = el.ser.apply(o);

                packer.packExtensionTypeHeader(code, typeid.length - 1 + res.length);
                packer.writePayload(typeid, 1, typeid.length-1);
                packer.writePayload(res);
                return;
            }
        }
        throw new RuntimeException("Unable to serialize: " + o.toString());
    }

    private static void test_obj(Object obj, String hex) {
        final byte[] b = serialize(obj);
        final Object obj2 = deserialize(b);
        assert (obj == null ? obj2 == null : obj2.equals(obj)) : obj2;
        assert hex==null || hex.equals(Utils.bytesToHex(b)) : Utils.bytesToHex(b);
    }

    public static void test() {
        Object none = null;
        Boolean bool = new Boolean(true);
        int integer = 13;
        double floating = 3.14;
        String string = "Hello";
        ByteBuffer bytes = ByteBuffer.wrap(Utils.hexToBytes("414243444546474849"));

        DStatus dstatus = DStatus.WAITING;
        DFunType dfuntype = DFunType.LAMBDA;
        DRef dref = new DRef("CAFE530A8ACAE736962C14AB34F1D6F8D913C5A8");
        DCall dcall = new DCall("func",new ArrayList<Object>(Arrays.asList(null, 1, 2.2)),13);
        Fiddles fiddles_empty = new Fiddles();
        Fiddle fiddle = new Fiddle(new DCall("func",new ArrayList<Object>(Arrays.asList(null, 1, 1.1)),13),null);
        Fiddle fiddle2 = new Fiddle(new DCall("fun2",new ArrayList<Object>(Arrays.asList(null, 2, 2.2)),14),1);
        Fiddle fiddle3 = new Fiddle(new DCall("fun3",new ArrayList<Object>(Arrays.asList(null, 3, 3.3)),15),2);
        Fiddles fiddles = new Fiddles();
        fiddles.add(fiddle).add(fiddle2).add(fiddle3);
        DNodeKeyRunning dnodekeyrunning = new DNodeKeyRunning("git.git#1234567",new DNodeKey(dcall,fiddles));

        List<Object> array = new ArrayList<Object>(3);
        array.add(none);
        array.add(bool);
        array.add(integer);
        array.add(floating);
        array.add(bytes);
        array.add(string);
        array.add(dstatus);
        array.add(dfuntype);
        array.add(dref);
        array.add(dcall);
        array.add(fiddles_empty);
        array.add(fiddle);
        array.add(fiddles);
        array.add(dnodekeyrunning);

        Set<Object> set = new HashSet<Object>();
        set.add(none);
        set.add(bool);
        set.add(integer);
        set.add(floating);
        set.add(bytes);
        set.add(string);
        //set.add(dstatus);
        //set.add(dfuntype);
        //set.add(dref);
        //set.add(dcall);
        //set.add(fiddle);

        
        Map<Object,Object> map = new HashMap<Object,Object>();
        map.put("none",none);
        map.put("bool",bool);
        map.put("integer",integer);
        map.put("floating",floating);
        map.put("bytes",bytes);
        map.put("string",string);
        map.put("dstatus",dstatus);
        map.put("dfuntype",dfuntype);
        map.put("dref",dref);
        map.put("dcall",dcall);
        map.put("fiddles_empty",fiddles_empty);
        map.put("fiddle",fiddle);
        map.put("fiddles",fiddles);
        map.put("dnodekeyrunning",dnodekeyrunning);

        test_obj(set, null);

        /*
        test_obj(none, "c0");
        test_obj(bool, "c3");
        test_obj(integer, "0d");
        test_obj(floating, "cb40091eb851eb851f");
        test_obj(string, "a548656c6c6f");
        test_obj(bytes, "c409414243444546474849");

        test_obj(dstatus,"d5407302");
        test_obj(dfuntype,"d5404600");
        test_obj(dref,"c7154052cafe530a8acae736962c14ab34f1d6f8d913c5a8");
        test_obj(dcall,"c714406393a466756e6393c001cb400199999999999a0d");
        */

        // test_obj(fiddles_empty,"d5404490");
        // test_obj(fiddle,"c71a406492c714406393a466756e6393c001cb3ff199999999999a0dc0");
        // test_obj(fiddles,"c74a404496c714406393a466756e3293c002cb400199999999999a0e01c714406393a466756e3393c003cb400a6666666666660f02c714406393a466756e6393c001cb3ff199999999999a0dc0");
        // test_obj(dnodekeyrunning,"c776404393af6769742e6769742331323334353637c714406393a466756e6393c001cb400199999999999a0dc74a404496c714406393a466756e3293c002cb400199999999999a0e01c714406393a466756e3393c003cb400a6666666666660f02c714406393a466756e6393c001cb3ff199999999999a0dc0");

        // test_obj(array,"9ec0c30dcb40091eb851eb851fc409414243444546474849a548656c6c6fd5407302d5404600c7154052cafe530a8acae736962c14ab34f1d6f8d913c5a8c714406393a466756e6393c001cb400199999999999a0dd5404490c71a406492c714406393a466756e6393c001cb3ff199999999999a0dc0c74a404496c714406393a466756e3293c002cb400199999999999a0e01c714406393a466756e3393c003cb400a6666666666660f02c714406393a466756e6393c001cb3ff199999999999a0dc0c776404393af6769742e6769742331323334353637c714406393a466756e6393c001cb400199999999999a0dc74a404496c714406393a466756e3293c002cb400199999999999a0e01c714406393a466756e3393c003cb400a6666666666660f02c714406393a466756e6393c001cb3ff199999999999a0dc0");
        // test_obj(set, "c77340539b0da548656c6c6fc0c3c409414243444546474849c714406393a466756e6393c001cb400199999999999a0dc7154052cafe530a8acae736962c14ab34f1d6f8d913c5a8c71a406492c714406393a466756e6393c001cb3ff199999999999a0dc0cb40091eb851eb851fd5404600d5407302");
        // test_obj(map, "8ea4626f6f6cc3a464726566c7154052cafe530a8acae736962c14ab34f1d6f8d913c5a8a46e6f6e65c0a56279746573c409414243444546474849a56463616c6cc776404393af6769742e6769742331323334353637c714406393a466756e6393c001cb400199999999999a0dc74a404496c714406393a466756e3293c002cb400199999999999a0e01c714406393a466756e3393c003cb400a6666666666660f02c714406393a466756e6393c001cb3ff199999999999a0dc0a6646363616c6cc714406393a466756e6393c001cb400199999999999a0da6666964646c65c71a406492c714406393a466756e6393c001cb3ff199999999999a0dc0a6737472696e67a548656c6c6fa764737461747573d5407302a7666964646c6573c74a404496c714406393a466756e3293c002cb400199999999999a0e01c714406393a466756e3393c003cb400a6666666666660f02c714406393a466756e6393c001cb3ff199999999999a0dc0a7696e74656765720da86466756e74797065d5404600a8666c6f6174696e67cb40091eb851eb851fad666964646c65735f656d707479d5404490");

        
        System.out.println("Msgpack.test passed");
    }
}

