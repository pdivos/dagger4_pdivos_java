package com.dagger4.worker;

public class Main 
{
	public static void main( String[] args ) {
        if(args[0].equals("test")) {
            com.dagger4.utils.Msgpack.test();
            com.dagger4.utils.TreeSerializer.test();
        } else if(args[0].equals("dcallables")) {
            for(DCallable dcallable : DFunCache.dcallables()) {
                System.out.print(dcallable.dcallable_hash());
                System.out.print(",");
                System.out.print(dcallable.dfuntype().name());
                System.out.print(",");
                System.out.print(dcallable.func());
                System.out.print(",");
                System.out.println(com.dagger4.utils.Utils.b64encode(dcallable.source()));
            }
        } else if(args[0].equals("worker")) {
            String server_host = args[1];
            server_host = "ws://"+server_host+":8000/dagger";
            String worker_repo = args[2];
            String worker_id = args[3];
            Worker.worker_loop(server_host, worker_id, worker_repo);
        } else {
            throw new RuntimeException("Invalid command line arg: " + args[0]);
        }
	}
}
