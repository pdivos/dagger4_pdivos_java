package com.dagger4.worker;

import java.util.List;

import com.pdivos_java.DCallables;
import com.dagger4.core.DFunType;
import com.dagger4.utils.Utils;

public class DFunCache {
    public static List<DCallable> dcallables() {
        return DCallables.dcallables();
    }
    public static DCallable dcallable(String func) {
        for(DCallable dcallable : DFunCache.dcallables()) {
            if(dcallable.func().equals(func)) {
                return dcallable;
            }
        }
        throw new RuntimeException("Couldn't find func: " + func);
    }
}
 