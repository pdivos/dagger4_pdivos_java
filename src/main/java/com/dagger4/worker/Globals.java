package com.dagger4.worker;

import com.dagger4.core.*;
import com.dagger4.utils.Client;
import com.dagger4.utils.DRefStore;

import java.util.*;

public class Globals {
    // home for all globals held by the single-threaded dagger process
    // dcall instance that is being executed right now, so we know what to register missing deps to
    public static DNodeKey dnodekey = null;

    // global client instance
    public static Client dclient = null;

    // stack of fiddles applied by the Fiddle context manager
    public static List<Fiddle> fiddle_stack = new ArrayList<Fiddle>();

    public static Map<String, DFunType> _func_dfuntypes = null;
    public static DFunType dfuntype(String func) {
        assert _func_dfuntypes.containsKey(func) : "Attempting to call unknown function: " + func;
        return _func_dfuntypes.get(func);
    }

    // child_to_value is a nested map in Java
    // in python its (dcall,fiddle) -> (is_success,value) but in Java no tuple
    public static Map<DNodeKey,DNodeResult> _child_to_value = null;

    public static String dcommitset = null;
}



