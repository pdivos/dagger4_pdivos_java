package com.dagger4.worker;

public class MissingDepException extends RuntimeException {
    public MissingDepException(String m) {
        super(m);
    }
}