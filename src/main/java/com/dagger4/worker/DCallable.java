package com.dagger4.worker;

import java.util.List;

import com.dagger4.core.DFunType;
import com.dagger4.utils.Utils;

public interface DCallable {
    public String func();
    public Object run(List<Object> args);
    public DFunType dfuntype();
    public String source();
    public default String dcallable_hash() {
        return com.dagger4.utils.Utils.bytesToHex(Utils.sha1(source().getBytes()));
    }
}