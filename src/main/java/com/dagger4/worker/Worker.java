package com.dagger4.worker;

import java.util.*;
import com.dagger4.core.*;
import com.dagger4.utils.Client;
import com.dagger4.utils.DRefStore;
import com.dagger4.utils.MyLog;

public class Worker {
    private enum Status {
        Succeed, Exception, MissingDep
    };
    public static void worker_loop(String server_host, String worker_id, String worker_repo) {
        worker_loop(server_host, worker_id, worker_repo, false, 1.0);
    }
    public static void worker_loop(String server_host, String worker_id, String worker_repo, boolean stop_on_done, double sleep_secs) {
        // worker_repo: drepo of the worker, defines the queue the worker will listen on. in theory this could be figured out by the worker itself but it"s easier to pass it in from the server
        // on_failed_dcallback: DEvent name of the dcall that should be dcalled on a failure
        //                     callback must accept following args:
        //                         dcall, stdout, stderr, source, stacktrace
        //                     if None then no dcall will be called

        Client client = new Client(worker_id, server_host);
        boolean running = true;
        // using client as KVStore. TODO: use something else like S3 or another service for heavy stuff so that DServer is not killed by this
        Globals.dclient = client;
        boolean waiting_for_work_logged = false;
        while(running) {
            Map<String,Object> resp = client.get_work(worker_repo, stop_on_done ? 0.0 : sleep_secs);
            if(resp == null) {
                if(stop_on_done) {
                    MyLog.d("no more work, exiting");
                    break;
                } else {
                    if(!waiting_for_work_logged) MyLog.d("waiting for work...");
                    waiting_for_work_logged = true;
                    continue;
                }
            }
            waiting_for_work_logged = false;
            assert resp.containsKey("dnodekeyrunning");
            DNodeKeyRunning dnodekeyrunning = (DNodeKeyRunning)resp.get("dnodekeyrunning");
            assert resp.containsKey("func_dfuntypes");
            Map<String,Integer> tmp = (Map<String,Integer>)client.treeserializer.deserialize((DRef)resp.get("func_dfuntypes"));
            Globals._func_dfuntypes = new HashMap<String,DFunType>();
            for(String func:tmp.keySet()) {
                assert tmp.containsKey(func);
                Globals._func_dfuntypes.put(func, DFunType.values()[tmp.get(func)]);
            }
            Globals._child_to_value = new HashMap<DNodeKey,DNodeResult>();
            assert resp.containsKey("child_to_value");
            List<Object> tmp2 = (List<Object>)resp.get("child_to_value");
            assert tmp2.size() % 2 == 0;
            for(int i=0; i<tmp2.size(); i+=2) {
                DNodeKey dnodekey = (DNodeKey)tmp2.get(i);
                DNodeResult dnoderesult = (DNodeResult)tmp2.get(i+1);
                Globals._child_to_value.put(dnodekey,dnoderesult);
            }

            MyLog.d("executing: " + dnodekeyrunning.toString());
            Status status = Worker.execute_dcall(client, dnodekeyrunning);

            Globals._child_to_value = null;
            Globals._func_dfuntypes = null;

            MyLog.d(status, dnodekeyrunning);
        }
        try {
            client.closeBlocking();
        } catch(InterruptedException e) {
            throw new RuntimeException(e);
        }
        Globals.dclient = null;
    }

    private static Status execute_dcall(Client client, DNodeKeyRunning dnodekeyrunning) {
        // executes a DNodeKeyRunning
        // dcall: dcall to execute to
        // olddeps: list of existing evaled deps of the node, needed to check if Eval can be called
        // cache: used by DFunction to access dep values
        // returns a DNode which contains the results of the run
        List<Object> args = client.treeserializer.resolve_list(dnodekeyrunning.dnodekey().dcall().args());
        Globals.dnodekey = dnodekeyrunning.dnodekey();
        Globals.dcommitset = dnodekeyrunning.dcommitset();
        assert Globals.fiddle_stack.size() == 0;
        // outcomes:
        //   Succeed: successful run with a valid return value
        //   Exception: fail during run with a valid stacktrace
        //   MissingDep: MissingDepException raised due to missing dep
        Status status;
        Object stdout = null;
        Object stderr = null;
        Object dexception = null;
        Object value = null;

        // https://stackoverflow.com/questions/8708342/redirect-console-output-to-string-in-java
        java.io.ByteArrayOutputStream stdout_baos = new java.io.ByteArrayOutputStream();
        java.io.PrintStream stdout_ps = new java.io.PrintStream(stdout_baos);
        java.io.ByteArrayOutputStream stderr_baos = new java.io.ByteArrayOutputStream();
        java.io.PrintStream stderr_ps = new java.io.PrintStream(stderr_baos);
        java.io.PrintStream stdout_old = System.out;
        java.io.PrintStream stderr_old = System.err;
        System.setOut(stdout_ps);
        System.setErr(stderr_ps);

        DCallable dcallable = DFunCache.dcallable(dnodekeyrunning.dnodekey().dcall().func());
        try {
            value = dcallable.run(args);
            value = client.treeserializer.ensure(value);
            status = Status.Succeed;
            assert Globals.fiddle_stack.size() == 0;
        } catch(DException e) {
            ArrayList<DNodeKey> dnodekeys = e.dnodekeys();
            dnodekeys.add(Globals.dnodekey);
            dexception = new DException(e.stacktrace(), dnodekeys);
            dexception = client.treeserializer.ensure(dexception);
            status = Status.Exception;
        } catch(MissingDepException e) {
            status = Status.MissingDep;
        } catch(Throwable e) {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            String stacktrace = sw.toString();
     		com.dagger4.utils.MyLog.d("fail", stacktrace);
            ArrayList<DNodeKey> dnodekeys = new ArrayList<DNodeKey>();
            dnodekeys.add(Globals.dnodekey);
            dexception = new DException(e.getMessage(), dnodekeys);
            dexception = client.treeserializer.ensure(dexception);
            status = Status.Exception;
        } finally {
            Globals.fiddle_stack = new ArrayList<Fiddle>(); // resetting to empty stack
            Globals.dnodekey = null;
            Globals.dcommitset = null;
            System.out.flush();
            System.setOut(stdout_old);
            System.err.flush();
            System.setErr(stderr_old);
            stdout = java.nio.ByteBuffer.wrap(stdout_baos.toByteArray());
            stderr = java.nio.ByteBuffer.wrap(stderr_baos.toByteArray());
            stdout = client.treeserializer.ensure(stdout);
            stderr = client.treeserializer.ensure(stderr);
        }

        if(status == Status.Succeed) {
            client.complete(dnodekeyrunning, true, value, stdout, stderr);
        } else if(status == Status.Exception) {
            client.complete(dnodekeyrunning, false, dexception, stdout, stderr);
        }
        return status;
    }
}


