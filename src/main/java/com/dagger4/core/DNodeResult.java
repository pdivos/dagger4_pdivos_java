package com.dagger4.core;

public class DNodeResult {
    private final Boolean is_success;
    private final Object value;
    private final Object stdout;
    private final Object stderr;
    public DNodeResult(Boolean _is_success, Object _value, Object _stdout, Object _stderr) {
        assert com.dagger4.utils.Utils.is_primitive(_value) || (_value instanceof DRef): _value;
        assert com.dagger4.utils.Utils.is_primitive(_stdout) || (_stdout instanceof DRef): _stdout;
        assert com.dagger4.utils.Utils.is_primitive(_stderr) || (_stderr instanceof DRef): _stderr;
        this.is_success = _is_success;
        this.value = _value;
        this.stdout = _stdout;
        this.stderr = _stderr;
    }
    public Boolean is_success() {
        return this.is_success;
    }
    public Object value() {
        return this.value;
    }
    public Object stdout() {
        return this.stdout;
    }
    public Object stderr() {
        return this.stderr;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DNodeResult))
            return false;
        if (obj == this)
            return true;
        DNodeResult o = (DNodeResult)obj;
        return this.is_success.equals(o.is_success) && this.value.equals(o.value) && this.stdout.equals(o.stdout) && this.stderr.equals(o.stderr);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.is_success, this.value, this.stdout, this.stderr);
    }
    @Override
    public String toString() {
        return "DNodeResult(" + this.is_success().toString()
                        + "," + this.value().toString()
                        + "," + (this.stdout() == null ? "null" : this.stdout().toString())
                        + "," + (this.stderr() == null ? "null" : this.stderr().toString()) + ")";
    }
}