package com.dagger4.core;

public class DServerError extends RuntimeException {
    public DServerError(String msg) {
        super(msg);
    }
}