package com.dagger4.core;

import java.util.*;

public class DException extends RuntimeException {
    private final ArrayList<DNodeKey> dnodekeys;
    public DException(String stacktrace, ArrayList<DNodeKey> dnodekeys) {
        super(stacktrace);
        this.dnodekeys = dnodekeys;
    }
    public String stacktrace() {
        return this.getMessage();
    }
    public ArrayList<DNodeKey> dnodekeys() {
        return this.dnodekeys;
    }
    @Override
    public String toString() {
        return "DException(" + stacktrace() + "," + dnodekeys().toString() + ")";
    }
}