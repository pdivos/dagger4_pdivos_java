package com.dagger4.core;

import java.util.List;
import java.util.ArrayList;

public class Fiddle {
    private final DCall dcall;
    private final Object value;
    public Fiddle(DCall _dcall, Object _value) {
        this.dcall = _dcall;
        this.value = _value;
        assert com.dagger4.utils.Utils.is_primitive(_value) || (_value instanceof DRef);
    }
    public DCall dcall() {
        return this.dcall;
    }
    public Object value() {
        return this.value;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Fiddle))
            return false;
        if (obj == this)
            return true;
        Fiddle o = (Fiddle)obj;
        return this.dcall.equals(o.dcall) && ((this.value == o.value) || this.value.equals(o.value));
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.dcall, this.value);
    }
    @Override
    public String toString() {
        return "Fiddle(" + this.dcall.toString() + ", " + (this.value == null ? "null" : this.value.toString()) + ")";
    }
}