package com.dagger4.core;

public enum DStatus {
    READY,
    RUNNING,
    WAITING,
    COMPLETED,
}