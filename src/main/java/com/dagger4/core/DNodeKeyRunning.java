package com.dagger4.core;

public class DNodeKeyRunning {
    private final String dcommitset;
    private final DNodeKey dnodekey;
    public DNodeKeyRunning(String _dcommitset, DNodeKey _dnodekey) {
        assert _dcommitset != null;
        this.dcommitset = _dcommitset;
        this.dnodekey = _dnodekey;
    }
    public String dcommitset() {
        return this.dcommitset;
    }
    public DNodeKey dnodekey() {
        return this.dnodekey;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DNodeKeyRunning))
            return false;
        if (obj == this)
            return true;
        DNodeKeyRunning o = (DNodeKeyRunning)obj;
        return this.dcommitset.equals(o.dcommitset) && this.dnodekey.equals(o.dnodekey);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.dcommitset, this.dnodekey);
    }
    @Override
    public String toString() {
        return "DNodeKeyRunning(" + this.dcommitset().toString()
                        + "," + this.dnodekey().toString() + ")";
    }
}