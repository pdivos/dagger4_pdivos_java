package com.dagger4.core;

import java.util.List;
import java.util.ArrayList;

public class DCall {
    private final String func;
    private final List<Object> args;
    private final long ts;
    public DCall(String _func, List<Object> _args, long _ts) {
        this.func = _func;
        this.args = _args;
        for(Object o : _args)
            assert com.dagger4.utils.Utils.is_primitive(o) || (o instanceof DRef): o;
        this.ts = _ts;
    }
    public String func() {
        return this.func;
    }
    public List<Object> args() {
        return this.args;
    }
    public long ts() {
        return this.ts;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DCall))
            return false;
        if (obj == this)
            return true;
        DCall o = (DCall)obj;
        return this.func.equals(o.func) && this.args.equals(o.args) && this.ts == o.ts;
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.func, this.args, this.ts);
    }
    @Override
    public String toString() {
        return "DCall(" + this.func()
                        + "," + this.args().toString()
                        + "," + new Long(this.ts()).toString() + ")";
    }
}