package com.dagger4.core;

public class DNodeKeyCompleted {
    private final DRef deep_dcallable_hash;
    private final DNodeKey dnodekey;
    public DNodeKeyCompleted(DRef _deep_dcallable_hash, DNodeKey _dnodekey) {
        this.deep_dcallable_hash = _deep_dcallable_hash;
        this.dnodekey = _dnodekey;
    }
    public DRef deep_dcallable_hash() {
        return this.deep_dcallable_hash;
    }
    public DNodeKey dnodekey() {
        return this.dnodekey;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DNodeKeyCompleted))
            return false;
        if (obj == this)
            return true;
        DNodeKeyCompleted o = (DNodeKeyCompleted)obj;
        return this.deep_dcallable_hash.equals(o.deep_dcallable_hash) && this.dnodekey.equals(o.dnodekey);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.deep_dcallable_hash, this.dnodekey);
    }
    @Override
    public String toString() {
        return "DNodeKeyCompleted(" + this.deep_dcallable_hash().toString()
                        + "," + this.dnodekey().toString() + ")";
    }
}