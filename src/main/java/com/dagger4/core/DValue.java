package com.dagger4.core;

import java.util.Set;
import com.dagger4.core.DRef;

public class DValue {
    private final byte[] typeid;
    private final byte[] buf;
    private final Set<DRef> drefs;
    public DValue(byte[] _typeid, byte[] _buf, Set<DRef> _drefs) {
        this.typeid = _typeid;
        this.buf = _buf;
        this.drefs = _drefs;
    }
    public byte[] typeid() {
        return this.typeid;
    }
    public byte[] buf() {
        return this.buf;
    }
    public Set<DRef> drefs() {
        return this.drefs;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DValue))
            return false;
        if (obj == this)
            return true;
        DValue o = (DValue)obj;
        return this.typeid.equals(o.typeid) && this.buf.equals(o.buf) && this.drefs.equals(o.drefs);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.typeid, this.buf, this.drefs);
    }
}