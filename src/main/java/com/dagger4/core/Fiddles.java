package com.dagger4.core;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.lang.Iterable;
import java.util.Iterator;

public class Fiddles implements Iterable<Fiddle> {
    public final List<Fiddle> fiddles;
    public Fiddles() {
        this.fiddles = new ArrayList<Fiddle>();
    }
    public Fiddles(List<Fiddle> _fiddles) {
        // note: sorting happens on serialization, see Msgpack.java
        Set<DCall> dcalls = new HashSet<DCall>();
        for(Fiddle f : _fiddles) {
            dcalls.add(f.dcall());
        }
        assert dcalls.size() == _fiddles.size();
        this.fiddles = _fiddles;
    }
    public Fiddles(Fiddles _fiddles) {
        this(_fiddles.fiddles);
    }
    public Fiddles add(Fiddle fiddle) {
        List<Fiddle> _fiddles = new ArrayList<Fiddle>();
        for(Fiddle f : this.fiddles) {
            if(!f.dcall().equals(fiddle.dcall()))
                _fiddles.add(f);
        }
        _fiddles.add(fiddle);
        return new Fiddles(_fiddles);
    }
    public Fiddles update(Fiddles fiddles) {
        Fiddles res = this;
        for(Fiddle f : fiddles.fiddles) {
            res = res.add(f);
        }
        return res;
    }
    public boolean contains(DCall dcall) {
        for(Fiddle f : this.fiddles)
            if(f.dcall().equals(dcall))
                return true;
        return false;
    }
    public Object get(DCall dcall) {
        for(Fiddle f : this.fiddles)
            if(f.dcall().equals(dcall))
                return f.value();
        throw new RuntimeException("key not found: " + dcall.toString());
    }
    public int size() {
        return this.fiddles.size();
    }
    @Override
    public Iterator<Fiddle> iterator() {
        return this.fiddles.iterator();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Fiddles)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Fiddles o = (Fiddles)obj;
        boolean ret = (this.fiddles).equals(o.fiddles);
        return ret;
    }
    @Override
    public int hashCode() {
        return this.fiddles.hashCode();
    }
}