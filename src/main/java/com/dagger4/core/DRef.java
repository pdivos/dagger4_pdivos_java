package com.dagger4.core;

import com.dagger4.utils.Utils;

public class DRef {
    public final byte[] ref;
    public DRef(byte[] dref) {
        this.ref = dref;
        assert this.ref.length == 20;
    }
    public DRef(String dref) {
        this.ref = Utils.hexToBytes(dref);
        assert this.ref.length == 20;
    }
    public static DRef from_buf(byte[] buf) {
        return new DRef(com.dagger4.utils.Utils.sha1(buf));
    }
    @Override
    public String toString() {
        return "DRef(" + Utils.bytesToHex(this.ref) + ")";
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof DRef)) {
            return false;
        }
        DRef user = (DRef) o;
        for(int i=0; i<20; i++) if (user.ref[i]!=this.ref[i]) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return java.util.Arrays.hashCode(this.ref);
    }
}