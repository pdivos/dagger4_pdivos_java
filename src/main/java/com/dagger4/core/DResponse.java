package com.dagger4.core;

import java.util.Map;

public class DResponse {
    private final Object response;
    private final Integer msg_id;
    private final Map<String,Object> __drefstore_s2c__;
    public DResponse(Object _response, Integer _msg_id, Map<String,Object> ___drefstore_s2c__) {
        this.response = _response;
        this.msg_id = _msg_id;
        this.__drefstore_s2c__ = ___drefstore_s2c__;
    }
    public Object response() {
        return this.response;
    }
    public Integer msg_id() {
        return this.msg_id;
    }
    public Map<String,Object> __drefstore_s2c__() {
        return this.__drefstore_s2c__;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DResponse))
            return false;
        if (obj == this)
            return true;
        DResponse o = (DResponse)obj;
        return this.response.equals(o.response) && this.msg_id.equals(o.msg_id) && this.__drefstore_s2c__.equals(o.__drefstore_s2c__);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.response, this.msg_id, this.__drefstore_s2c__);
    }
    @Override
    public String toString() {
        return "DResponse(" + this.response().toString()
                        + "," + this.msg_id().toString()
                        + "," + this.__drefstore_s2c__().toString()
                        + ")";
    }
}