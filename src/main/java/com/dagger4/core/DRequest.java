package com.dagger4.core;

import java.util.List;
import java.util.Map;

public class DRequest {
    private final String method;
    private final List<Object> args;
    private final Long msg_id;
    private final Map<String,Object> __drefstore_c2s__;
    public DRequest(String _method, List<Object> _args, Long _msg_id, Map<String,Object> ___drefstore_c2s__) {
        this.method = _method;
        this.args = _args;
        this.msg_id = _msg_id;
        this.__drefstore_c2s__ = ___drefstore_c2s__;
    }
    public String method() {
        return this.method;
    }
    public List<Object> args() {
        return this.args;
    }
    public Long msg_id() {
        return this.msg_id;
    }
    public Map<String,Object> __drefstore_c2s__() {
        return this.__drefstore_c2s__;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DRequest))
            return false;
        if (obj == this)
            return true;
        DRequest o = (DRequest)obj;
        return this.method.equals(o.method) && this.args.equals(o.args) && this.msg_id.equals(o.msg_id) && this.__drefstore_c2s__.equals(o.__drefstore_c2s__);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.method, this.args, this.msg_id, this.__drefstore_c2s__);
    }
    @Override
    public String toString() {
        return "DRequest(" + this.method().toString()
                        + "," + this.args().toString()
                        + "," + this.msg_id().toString()
                        + "," + this.__drefstore_c2s__().toString()
                        + ")";
    }
}