package com.dagger4.core;

public class DNodeKey {
    private final DCall dcall;
    private final Fiddles fiddles;
    public DNodeKey(DCall _dcall, Fiddles _fiddles) {
        this.dcall = _dcall;
        this.fiddles = _fiddles;
    }
    public DCall dcall() {
        return this.dcall;
    }
    public Fiddles fiddles() {
        return this.fiddles;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof DNodeKey))
            return false;
        if (obj == this)
            return true;
        DNodeKey o = (DNodeKey)obj;
        return this.dcall.equals(o.dcall) && this.fiddles.equals(o.fiddles);
    }
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.dcall, this.fiddles);
    }
    @Override
    public String toString() {
        return "DNodeKey(" + this.dcall().toString()
                     + "," + this.fiddles().toString() + ")";
    }
}