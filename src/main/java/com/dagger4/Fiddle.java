// emulate python context manager in java
// https://stackoverflow.com/questions/2945359/simulating-pythons-with-statement-in-java
// https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html

// this is how to use:
// try (new Fiddle("fib", [19], 4893)) {
//     call("fib", [20]);
// }

// TODO: it's confusing that we have a core.Fiddle and also a worker.Fiddle

package com.dagger4;

import java.util.List;
import java.util.ArrayList;
import com.dagger4.worker.Globals;
import com.dagger4.core.DFunType;

public class Fiddle implements java.lang.AutoCloseable {
    private final com.dagger4.core.Fiddle fiddle;
    public Fiddle(String func, List<Object> args, Object value, long ts) {
        if(Globals.dfuntype(func) == DFunType.LAMBDA) ts = 0;
        this.fiddle = new com.dagger4.core.Fiddle(new com.dagger4.core.DCall(func, args, ts), value);
        Globals.fiddle_stack.add(this.fiddle);
    }
    public Fiddle(String func, Object ... args_and_value) {
        this(func, Fiddle.get_args(args_and_value), args_and_value[args_and_value.length-1], Globals.dnodekey.dcall().ts());
    }
    private static List<Object> get_args(Object[] args_and_value) {
        assert args_and_value.length >= 1;
        List<Object> args = new ArrayList<Object>(args_and_value.length-1);
        for(int i=0; i<args_and_value.length-1; i++) {
            args.add(args_and_value[i]);
        }
        return args;
    }
    @Override
    public void close() {
        com.dagger4.core.Fiddle _ = Globals.fiddle_stack.remove(Globals.fiddle_stack.size() - 1);
        assert _.equals(this.fiddle);
    }
}