package com.dagger4;

import java.util.*;
import com.dagger4.core.*;
import com.dagger4.utils.Client;
import com.dagger4.utils.DRefStore;
import com.dagger4.worker.Globals;
import com.dagger4.worker.MissingDepException;

public class Call {
    public final Object value;
    private static long TS_UNSPECIFIED = Long.MIN_VALUE;

    private static DNodeKey _callee(String func, long ts, List<Object> args) {
        // "constructs callee dcall"
        DNodeKey caller = Globals.dnodekey;
        DCall dcall = caller.dcall();
        // inherit timestamp from caller if not explicitly provided. even for LAMBDAS, zeroed out on server side
        ts = (ts == TS_UNSPECIFIED ? caller.dcall().ts() : ts);
        if(Globals.dfuntype(func) == DFunType.LAMBDA) {
            ts = 0;
        } else {
            assert Globals.dfuntype(caller.dcall().func()) == DFunType.FUNCTION : "FUNCTION can only be called from FUNCTION";
        }
        // get fiddles inherited from caller
        Fiddles fiddles = new Fiddles(caller.fiddles());
        // apply fiddles added by Fiddle context manager
        for(com.dagger4.core.Fiddle d : Globals.fiddle_stack) {
            fiddles.add(d);
        }
        DNodeKey callee = new DNodeKey(new DCall(func, args, ts), fiddles);
        // MyLog.d("_callee", func, args, caller, callee);
        return callee;
    }
    public Call(String func, Object ... args) {
        this(TS_UNSPECIFIED, func, args);
    }
    public Call(long ts, String func, Object ... _args) {
        Client client = Globals.dclient;
        DNodeKey caller = Globals.dnodekey;
        com.dagger4.utils.MyLog.d("call", caller);
        List<Object> args = client.treeserializer.ensure_list(_args);
        DNodeKey callee = Call._callee(func, ts, args);
        if(caller.fiddles().contains(callee.dcall())) {
            this.value = client.treeserializer.resolve(caller.fiddles().get(callee.dcall()));
        } else {
            if(Globals._child_to_value.containsKey(callee)) {
                DNodeResult ret = Globals._child_to_value.get(callee);
                Boolean is_success = ret.is_success();
                Object value = ret.value();
                value = client.treeserializer.resolve(value);
                if(is_success) {
                    this.value = value;
                } else {
                    assert value instanceof DException : value.getClass();
                    throw (DException)value;
                }
            } else {
                Map<String,Object> ret = client.call(new DNodeKeyRunning(Globals.dcommitset, callee), new DNodeKeyRunning(Globals.dcommitset, caller));
                assert ret.containsKey("dstatus");
                if(ret.get("dstatus").equals(DStatus.COMPLETED)) {
                    assert ret.containsKey("result");
                    DNodeResult result = (DNodeResult)ret.get("result");
                    Object value = client.treeserializer.resolve(result.value());
                    if(result.is_success()) {
                        this.value = value;
                    } else {
                        assert value instanceof DException : value.getClass();
                        throw (DException)value;
                    }
                } else {
                    throw new MissingDepException("missing dep: " + callee.toString());
                }
            }
        }
    }
}