#!/usr/bin/env python3

def _run_cmd(cmd, assert_succeed = True):
    import subprocess
    p = subprocess.run(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    stdout = p.stdout.decode('utf-8')
    stderr = p.stderr.decode('utf-8')
    exitcode = p.returncode
    ret = {'stdout': stdout, 'stderr': stderr, 'exitcode': exitcode}
    if assert_succeed:
        assert ret['exitcode'] == 0, ret['stdout'] + '\n' + ret['stderr']
    return ret

def build():
    import os
    repo_root = os.path.dirname(os.path.abspath(__file__)) + '/'
    assert os.path.isdir(repo_root)
    os.chdir(repo_root)

    ret = _run_cmd("docker build .")
    assert 'Successfully built ' in ret['stdout']
    image_id = ret['stdout'].split('Successfully built ')[-1].strip()
    return {'stdout': ret['stdout'], 'stderr': ret['stderr'], 'image_id': image_id}

def is_built(image_id):
    ret = _run_cmd("docker images -q " + image_id)
    if image_id in ret['stdout'].split('\n'):
        return 1
    else:
        return 0

def remove(image_id):
    return _run_cmd("docker image rm " + image_id, assert_succeed=False)

def dcallables(image_id):
    cmd = 'docker run --rm '+image_id+' mvn exec:java -q -Dexec.mainClass="com.dagger4.worker.Main" -Dexec.args="dcallables"'
    ret = _run_cmd(cmd)
    return ret['stdout']

def run_worker(image_id, server_host, worker_repo, logs_folder):
    """
    starts a worker

    image_id: which image to run
    server_host: dagger4 server ip address
    worker_repo: dcommit of the worker, defines the queue the worker will listen on. in theory this could be figured out by the worker itself but it's easier to pass it in from the server
    logs_folder: local path where logs shall be saved
    """
    cmd = ""
    cmd += 'mkdir -p '+logs_folder+'; '                     # create logs folder on host if not exists
    cmd += 'docker run --rm -d '                            # docker run
    cmd += '-v '+logs_folder+':/logs '                      # attach volume
    cmd += '--network=host '+image_id+' bash -c '
    cmd +=  '\''
    cmd +=      'export CONTAINER_ID=$(cat /proc/self/cgroup | grep docker/ | tail -1 | cut -d"/" -f3); ' # get running container ID
    cmd +=      'mvn exec:java -X -Dexec.mainClass="com.dagger4.worker.Main" -Dexec.args="worker '+server_host+' '+worker_repo+' $CONTAINER_ID"'     # start worker and pass import statement that triggers importing dcallables from local repo
    cmd +=      '> /logs/$CONTAINER_ID.out 2>/logs/$CONTAINER_ID.err' # redirect outputs to files named by container_id
    cmd +=  '\''
    ret = _run_cmd(cmd)
    container_id = str(ret['stdout']).strip()
    return container_id

def list_workers(image_id):
    ret = _run_cmd('docker ps --quiet --filter "id='+image_id+'"')
    return [el for el in ret['stdout'].split('\n') if el != '']

def stop_worker(container_id):
    return _run_cmd("docker kill ----signal=SIGINT " + container_id, assert_succeed=False)['exitcode'] == 0

def kill_worker(container_id):
    return _run_cmd("docker kill " + container_id, assert_succeed=False)['exitcode'] == 0

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        print("Error: at least one arg needed", file=sys.stderr)
        sys.exit(1)
    arg = sys.argv[1]
    if arg == "build":
        assert len(sys.argv) == 2
        ret = build()
        print(ret['stdout'], file=sys.stdout, end='')
        print(ret['image_id'], file=sys.stdout) # last line printed on stdout must be the image id
        print(ret['stderr'], file=sys.stderr, end='')
    elif arg == "is_built":
        assert len(sys.argv) == 3
        image_id = sys.argv[2]
        print(is_built(image_id), file=sys.stdout, end='')
    elif arg == "remove":
        assert len(sys.argv) == 3
        image_id = sys.argv[2]
        ret = remove(image_id)
        print(ret['stderr'], file=sys.stderr, end='')
        print(ret['stdout'], file=sys.stdout)
    elif arg == "dcallables":
        assert len(sys.argv) == 3
        image_id = sys.argv[2]
        print(dcallables(image_id),end='')
    elif arg == "run_worker":
        assert len(sys.argv) == 6
        image_id = sys.argv[2]
        server_host = sys.argv[3]
        worker_repo = sys.argv[4]
        logs_folder = sys.argv[5]
        print(run_worker(image_id, server_host, worker_repo, logs_folder))
    elif arg == "list_workers":
        assert len(sys.argv) == 3
        image_id = sys.argv[2]
        for el in list_workers(image_id):
            print(el)
    elif arg == "stop_worker":
        assert len(sys.argv) == 3
        container_id = sys.argv[2]
        print(stop_worker(container_id))
    elif arg == "kill_worker":
        assert len(sys.argv) == 3
        container_id = sys.argv[2]
        print(kill_worker(container_id))
    else:
        print("Error: unknown arg: " + arg, file=sys.stderr)
        sys.exit(1)
    sys.exit(0)
